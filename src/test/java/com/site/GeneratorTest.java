/**
 * Copyright (c) 2022-2023, Mybatis-Flex (fuhai999@gmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.site;

import com.mybatisflex.codegen.Generator;
import com.mybatisflex.codegen.config.GlobalConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.junit.jupiter.api.Test;

public class GeneratorTest {


    @Test
    public void testGenerator() {
        //配置数据源
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setJdbcUrl("jdbc:mysql://192.168.110.111:3306/rock?characterEncoding=utf-8");
        dataSource.setUsername("root");
        dataSource.setPassword("root");



        GlobalConfig globalConfig = new GlobalConfig();
        globalConfig.setSourceDir(System.getProperty("user.dir") + "/src/main/java");
        globalConfig.setTablePrefix("t_");
        globalConfig.setEntityWithLombok(true);

        //设置只生成哪些表
        globalConfig.setGenerateTable("t_generation");

        //设置 entity 的包名
        globalConfig.setEntityGenerateEnable(true);
        globalConfig.setEntityClassSuffix("Entity");
        globalConfig.setEntityPackage("com.site.entity");

        //是否生成 mapper 类，默认为 false
        globalConfig.setMapperGenerateEnable(true);
        globalConfig.setMapperClassSuffix("Mapper");
        globalConfig.setMapperPackage("com.site.mapper");

        globalConfig.setServiceGenerateEnable(true);
        globalConfig.setServiceClassSuffix("Service");
        globalConfig.setServicePackage("com.site.service");

        globalConfig.setServiceImplGenerateEnable(true);
        globalConfig.setServiceImplClassSuffix("ServiceImpl");
        globalConfig.setServiceImplPackage("com.site.service.impl");


        globalConfig.setEntityGenerateEnable(true);
        globalConfig.setEntityWithLombok(true);

        //通过 datasource 和 globalConfig 创建代码生成器
        Generator generator = new Generator(dataSource, globalConfig);

        //开始生成代码
        generator.generate();
    }
}
