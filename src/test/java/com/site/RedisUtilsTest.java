package com.site;

import com.site.utils.RedisUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.noear.solon.annotation.Inject;
import org.noear.solon.test.SolonJUnit5Extension;
import org.noear.solon.test.SolonTest;


@ExtendWith(SolonJUnit5Extension.class)
@SolonTest(Main.class)
public class RedisUtilsTest {

    @Inject
    RedisUtils redisUtils;

    @Test
    public void add(){
        redisUtils.put("a","aa",51111);
    }

    @Test
    public void check() {
//        ONode oNode = redisUtils.get("a");
        String a = redisUtils.getStr("a");
        System.out.println(a);
    }


}
