package com.site;

import com.mybatisflex.core.util.StringUtil;
import org.dromara.hutool.core.data.id.IdUtil;
import org.junit.jupiter.api.Test;

public class StrTest {
    @Test
    public void test(){
        String key = "sys_user";
        String s = StringUtil.underlineToCamel(key);
        System.out.println(s);
        String s1 = StringUtil.camelToUnderline(s);
        System.out.println(s1);
    }

    @Test
    public void testId(){
        long dataCenterId = IdUtil.getSnowflakeNextId();
        System.out.println(dataCenterId);


    }
}
