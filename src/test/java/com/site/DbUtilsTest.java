package com.site;

import com.mybatisflex.core.row.Db;
import com.mybatisflex.core.row.Row;
import com.site.utils.DbUtils;
import com.site.utils.IPage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.noear.solon.annotation.Inject;
import org.noear.solon.core.handle.Result;
import org.noear.solon.test.SolonJUnit5Extension;
import org.noear.solon.test.SolonTest;

import java.util.List;
import java.util.Map;


@ExtendWith(SolonJUnit5Extension.class)
@SolonTest(Main.class)
public class DbUtilsTest {

    @Inject
    DbUtils dbUtils;

    @Test
    public void  add(){
        Row row = new Row();
        row.set("name", "michael yang");
        row.set("department_id", 18);
        dbUtils.add("sys_user", row);
    }


    @Test
    public void  put(){
        Row row = new Row();
        row.set("id", 1);
        row.set("name", "michael yang");
        row.set("department_id", 18);
        dbUtils.put("sys_user", row);
    }

    @Test
    public void getAll(){
        List<Map<String, Object>> userRole = dbUtils.getAll("user_role");
        System.out.println(userRole);
    }

    @Test
    public void TestPage(){
        IPage page = new IPage();
        Row eq = new Row();
        Row like=new Row();
        like.set("name", "li");
        page.setLike(like);
        Result r = dbUtils.page("sysUser", page);
        System.out.println(r);

    }
}
