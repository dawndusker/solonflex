package com.site;


import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.site.entity.ElectricEntity;
import com.site.entityVO.ElectricVO;
import com.site.mapper.ElectricMapper;
import org.apache.ibatis.solon.annotation.Db;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.noear.solon.test.HttpTester;
import org.noear.solon.test.SolonJUnit5Extension;
import org.noear.solon.test.SolonTest;
import java.util.List;

import static com.mybatisflex.core.query.QueryMethods.*;
import static com.site.entity.table.ElectricEntityTableDef.ELECTRIC_ENTITY;
import static com.site.entity.table.GenerationEntityTableDef.GENERATION_ENTITY;

@ExtendWith(SolonJUnit5Extension.class)
@SolonTest(Main.class)
public class ElectricTest  extends HttpTester {

    @Test
    public void test(){
        System.out.println(1);
    }
    @Db
    private ElectricMapper electricMapper;

    @Test
    public void  testJoin(){
        QueryWrapper query = QueryWrapper.create()
                .select(ELECTRIC_ENTITY.ALL_COLUMNS)
                .select(GENERATION_ENTITY.GENERATION_TYPE)
                .from(ELECTRIC_ENTITY)
                .leftJoin(GENERATION_ENTITY).on(ELECTRIC_ENTITY.TYPE_ID.eq(GENERATION_ENTITY.ID));
        List<ElectricVO> results = electricMapper.selectListByQueryAs(query, ElectricVO.class);
        System.out.println(results);
    }

    @Test
    public void testAll(){
        electricMapper.selectAll();
    }

    @Test
    public void testPage(){
        Page<ElectricEntity> paginate = electricMapper.paginate(1, 5, QueryWrapper.create());
    }

    @Test
    public void testAvg(){
        QueryWrapper query = new QueryWrapper()
                .select(
                        ELECTRIC_ENTITY.TYPE,
                        max(ELECTRIC_ENTITY.POWER).as("powerMax"),
                        sum(ELECTRIC_ENTITY.POWER).as("powerSum"),
                        avg(ELECTRIC_ENTITY.POWER).as("powerAvg")
                ).from(ELECTRIC_ENTITY)
                .groupBy(ELECTRIC_ENTITY.TYPE);
        List<ElectricEntity> electricEntities = electricMapper.selectListByQuery(query);
        electricEntities.forEach( System.out::println);
    }

    @Test
    public void testOne(){
//        ElectricEntity electricEntity = electricMapper.selectOneById(1);
        QueryWrapper query = new QueryWrapper();
        query.where(ELECTRIC_ENTITY.ID.eq(1));
        ElectricEntity electricEntity1 = electricMapper.selectOneByQuery(query);
        System.out.println(electricEntity1);
    }

    @Test
    public void testArray(){
        QueryWrapper query = new QueryWrapper();
        query.where(ELECTRIC_ENTITY.ID.le(11));
        List<ElectricEntity> electricEntities = electricMapper.selectListByQuery(query);
        electricEntities.forEach( System.out::println);

    }


}
