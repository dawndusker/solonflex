package com.site.esearchx;


import com.site.entity.ElectricEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.noear.esearchx.EsContext;
import org.noear.snack.ONode;
import org.noear.solon.annotation.Inject;
import org.noear.solon.test.SolonJUnit5Extension;

import java.io.IOException;

@ExtendWith(SolonJUnit5Extension.class)
public class EsSearchTest {
    private final String indexName = "t_electric";
//    @Inject("${url.es}")
    EsContext context;
    @Test
    public void test() throws IOException {
        String json = context.indice(indexName).selectJson();
        ONode node = ONode.loadStr(json);
        System.out.println(node);
    }

    @Test
    public void testGet() throws IOException {
        ElectricEntity index = context.indice(indexName).selectById(ElectricEntity.class, "EhMEPogBXDU9SfavaEmR");
        System.out.println(index);
        index.setPower(1);
        context.indice(indexName).upsert("EhMEPogBXDU9SfavaEmR", index);
    }


}
