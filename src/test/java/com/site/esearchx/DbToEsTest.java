package com.site.esearchx;

import com.mybatisflex.core.row.Db;
import com.mybatisflex.core.row.Row;
import com.site.Main;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.noear.esearchx.EsContext;
import org.noear.esearchx.model.EsData;
import org.noear.snack.ONode;
import org.noear.solon.annotation.Inject;
import org.noear.solon.test.SolonJUnit5Extension;
import org.noear.solon.test.SolonTest;

import java.io.IOException;
import java.util.List;


@ExtendWith(SolonJUnit5Extension.class)
@SolonTest(Main.class)
public class DbToEsTest {

//    @Inject("${url.es}")
    EsContext esx;


    @Test
    public void  testDbToEs() throws IOException {
      List<Row> tElectric = Db.selectAll("t_electric");
      esx.indice("t_electric").insertList(tElectric);
    }

    @Test
    public void testSearchById() throws IOException {
        ONode result = esx.indice("t_electric").selectById(ONode.class, "EhMEPogBXDU9SfavaEmR");
        System.out.println(result);
    }


    @Test
    public void testSearch() throws IOException {
        EsData<ONode> result = esx.indice("t_electric")
                .where(r -> r.term("type.keyword", "风力发电"))
                .limit(11)
                .selectList(ONode.class);
        System.out.println(result);
    }


}
