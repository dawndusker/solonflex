package com.site;

import com.site.utils.RedisUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.noear.redisx.RedisClient;
import org.noear.snack.ONode;
import org.noear.solon.annotation.Get;
import org.noear.solon.annotation.Inject;
import org.noear.solon.test.SolonJUnit5Extension;
import org.noear.solon.test.SolonTest;


@ExtendWith(SolonJUnit5Extension.class)
@SolonTest(Main.class)
public class RedisTest {
    @Inject
    RedisClient redisClient;


    @Test
    public void check() {
        String s = redisClient.getBucket().get("{CATEGORY_THIRD}_");
        System.out.println(s);
    }

    @Test
    public void delete(){
        redisClient.getBucket().remove("{CATEGORY_THIRD}_");
    }


}
