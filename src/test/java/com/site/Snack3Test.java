package com.site;

import com.site.entity.ElectricEntity;
import org.junit.jupiter.api.Test;
import org.noear.snack.ONode;
import org.noear.solon.test.SolonTest;

import java.time.LocalDate;

@SolonTest(Main.class)
public class Snack3Test {

    @Test
    public void test(){
        ElectricEntity electricEntity = new ElectricEntity();
        electricEntity.setId(1);
        electricEntity.setPower(111);
        electricEntity.setDate(LocalDate.now());
//        String json = ONode.stringify(electricEntity);
//        System.out.println(json);
//
//        int i = ONode.load(json).get("id").getInt();
//        String str = ONode.load(json).get("date").getString();
//        System.out.println(str);

        ONode oNode = ONode.load(electricEntity);
        String date = oNode.get("date").getString();
        System.out.println(date);

    }
}

