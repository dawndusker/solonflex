package com.site;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.core.row.Db;
import com.mybatisflex.core.row.Row;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.noear.solon.test.SolonJUnit5Extension;
import org.noear.solon.test.SolonTest;

import java.time.LocalDate;
import java.util.List;


@ExtendWith(SolonJUnit5Extension.class)
@SolonTest(Main.class)
public class DBTest {
   @Test
    public void  testDb(){
       // Row row= Db.selectOneById("t_electric","id",1000);
       // System.out.println(row);
       // Db.updateById("t_electric", Row.ofKey("id",1000).set("power",88));
       Row newRow=Db.selectOneById("t_electric","id",1000);
       System.out.println(newRow);
   }

   @Test
    public void  testInsert(){
       String sql="insert into t_electric(id,type,power,date) value (?, ?,?,?)";
       Db.insertBySql(sql,1000,"michael",11,LocalDate.now());
   }

   @Test
   public void  testDelete(){
       Db.deleteById("t_electric","id",1000);
   }

   @Test
   public void  testAdd(){
      String sql="insert into sys_user(id,name,department_id,create_time) value (?, ?,?,?)";
      Db.insertBySql(sql,12,"michael",11,LocalDate.now());
   }

   @Test
   public void  add(){
      Row row = new Row();
      row.set("name", "michael yang");
      row.set("department_id", 18);
//      row.set("create_time", new Date());
      System.out.println(row);
      Db.insert("sys_user", row);
   }

   @Test
   public void  testDbUser(){
      Row row= Db.selectOneById("sys_user","id",1);
      System.out.println(row.toCamelKeysMap());

   }

   @Test
   public void  testPage(){
      Page<Row> rowPage = Db.paginate("sys_user", 1, 10, QueryWrapper.create().orderBy("id"));
      System.out.println(rowPage);


   }

   @Test
   public void  testQueryLike(){
      QueryWrapper queryWrapper=QueryWrapper.create();
      QueryColumn queryColumn=new QueryColumn("name");
      queryColumn.like("li");
      queryWrapper.select(queryColumn);
      List<Row> sysUser = Db.selectListByQuery("sys_user", queryWrapper);
      System.out.println(sysUser);
   }

   @Test
   public void testInsertRow(){
      String table="department";
      Row row = Db.selectOneById(table, "id", 11);
      System.out.println(row);
      row.set("id", 12);
      Db.insert(table, row);

   }

}
