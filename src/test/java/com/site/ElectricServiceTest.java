package com.site;

import com.mybatisflex.core.query.QueryWrapper;
import com.site.entity.ElectricEntity;
import com.site.entity.GenerationEntity;
import com.site.entityVO.ElectricVO;
import com.site.service.ElectricService;
import com.site.service.GenerationService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.noear.solon.annotation.Inject;
import org.noear.solon.data.annotation.Tran;
import org.noear.solon.test.SolonJUnit5Extension;
import org.noear.solon.test.SolonTest;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.site.entity.table.ElectricEntityTableDef.ELECTRIC_ENTITY;
import static com.site.entity.table.GenerationEntityTableDef.GENERATION_ENTITY;


@ExtendWith(SolonJUnit5Extension.class)
@SolonTest(Main.class)
public class ElectricServiceTest {
    @Inject
    ElectricService electricService;

    @Inject
    GenerationService generationService;


    @Test
    @Tran
    public void  testGroup(){
        QueryWrapper queryWrapper=QueryWrapper.create()
                .select(ELECTRIC_ENTITY.TYPE)
                .from(ELECTRIC_ENTITY)
                .groupBy(ELECTRIC_ENTITY.TYPE);
        List<ElectricEntity> list=electricService.list(queryWrapper);
        for (ElectricEntity electricEntity : list) {
            GenerationEntity generationEntity=new GenerationEntity();
            generationEntity.setGenerationType(electricEntity.getType());
            generationService.save(generationEntity);
            electricEntity.setTypeId(generationEntity.getId());
        }
    }
    @Test
    public void  testType(){
        List<GenerationEntity>  generationList=generationService.list();
        List<ElectricEntity> electricList=electricService.list();
        for (ElectricEntity electricEntity : electricList) {
            Optional<GenerationEntity> first = generationList.stream().filter(item -> item.getGenerationType().equals(electricEntity.getType())).findFirst();
            first.ifPresent(generationEntity -> electricEntity.setTypeId(generationEntity.getId()));
        }
        electricService.updateBatch(electricList);
    }


    @Test
    public void  testAdd(){
        ElectricEntity electricEntity = new ElectricEntity();
        electricEntity.setId(1000);
        electricEntity.setType("aa");
        electricEntity.setDate(LocalDate.now());
        electricEntity.setPower(111);
        electricService.save(electricEntity);
    }

    @Test
    public void testUpdate(){
//        QueryWrapper queryWrapper = new QueryWrapper();
//        queryWrapper.where(ELECTRIC_ENTITY.TYPE.eq("aa"));
        ElectricEntity electricEntity = new ElectricEntity();
        electricEntity.setId(1000);
        electricEntity.setType("bb");
        electricEntity.setDate(LocalDate.now());
        electricEntity.setPower(111);
        electricService.update(electricEntity,ELECTRIC_ENTITY.TYPE.eq("aa"));
    }

    @Test
    public void  testDelete(){
        boolean bb = electricService.remove(ELECTRIC_ENTITY.TYPE.eq("aa"));
        System.out.println(bb);
    }

    @Test
    public void testGet(){
        ElectricEntity electricEntity = electricService.getById(1000);
        System.out.println(electricEntity);
    }

}
