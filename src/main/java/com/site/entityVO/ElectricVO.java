package com.site.entityVO;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ElectricVO implements Serializable {

    private Integer id;

    private String type;
    
    private Integer power;

    private Long typeId;

    private String generationType;

    private LocalDate date;
}
