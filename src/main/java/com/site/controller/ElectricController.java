package com.site.controller;

import com.mybatisflex.core.paginate.Page;
import com.site.entity.ElectricEntity;
import com.site.service.ElectricService;
import org.noear.solon.annotation.*;
import org.noear.solon.core.handle.Result;
import org.noear.solon.data.annotation.Cache;

@Controller
@Singleton(false)
@Mapping("/electric")
public class ElectricController {

    @Inject
    ElectricService electricService;

    @Mapping("page")
    public Result list(Page page,ElectricEntity electricEntity){
        return Result.succeed(electricService.pageQuery(page,electricEntity));
    }

    @Mapping("math")
    @Cache(key = "math",seconds = 20)
    public Result math(){
        return Result.succeed(electricService.math());
    }


    @Cache(key = "electric_${id}")
    @Mapping("{id}")
    public Result getId(@Param("id") Integer id){
        return Result.succeed(electricService.getById(id));
    }


}
