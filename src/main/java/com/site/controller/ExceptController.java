package com.site.controller;

import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.annotation.Param;
import org.noear.solon.annotation.Singleton;
import org.noear.solon.core.handle.Result;

@Controller
@Singleton(false)
@Mapping("/exception")
public class ExceptController {
    @Mapping("/error/{id}")
    public Result math(@Param("id") Integer id){
        if (id>10){
            throw  new RuntimeException("www");
        }
        return Result.failure();
    }
}
