package com.site.controller;


import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.row.Row;
import com.site.utils.DbUtils;
import com.site.utils.IPage;
import org.noear.solon.annotation.*;
import org.noear.solon.core.handle.Result;

import java.util.List;
import java.util.Map;

@Controller
@Singleton(false)
@Mapping("db")
public class DbController {
    @Inject
    DbUtils dbUtils;

    @Mapping("{table}/all")
    public Result<List<Map<String, Object>>> all(String table){
       return Result.succeed(dbUtils.getAll(table));
    }

    @Mapping("{table}/page")
    public Result<Page<Row>> page(String table, IPage page){
        return dbUtils.page(table,page);
    }

    @Mapping("{table}/one/{id}")
    public Result<Row> get(String table,Integer id){
        return dbUtils.get(table, id);
    }

    @Post
    @Mapping("{table}")
    public Result<Object> add(String table, Row row){
        return dbUtils.add(table,row);
    }

    @Put
    @Mapping("{table}")
    public Result<String> put(String table, Row row){
        return dbUtils.put(table,row);
    }


    @Delete
    @Mapping("{table}/{id}")
    public Result<String> delete(String table,Integer id){
        return dbUtils.delete(table, id);
    }
}
