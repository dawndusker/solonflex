package com.site.controller;


import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.row.Row;
import com.site.utils.EsUtils;
import com.site.utils.IPage;
import org.noear.snack.ONode;
import org.noear.solon.annotation.*;
import org.noear.solon.core.handle.Result;

import java.util.List;

@Controller
@Singleton(false)
@Mapping("es")
public class EsController {
    @Inject
    EsUtils esUtils;

    @Mapping("{index}/all")
    public Result all(String index){
       return esUtils.getAll(index);
    }

    @Mapping("{index}/page")
    public Result<Page<Row>> page(String index, IPage page){
        return esUtils.page(index,page);
    }

    @Get
    @Mapping("{index}/{id}")
    public Result<ONode> get(String index, String id){
        return esUtils.get(index, id);
    }

    @Post
    @Mapping("{index}")
    public Result<String> add(String index, ONode node){
        return esUtils.add(index,node);
    }

    @Put
    @Mapping("{index}")
    public Result<String> put(String index,ONode node){
        return esUtils.put(index,node);
    }


    @Delete
    @Mapping("{index}/{id}")
    public Result<String> delete(String index,String id){
        return esUtils.delete(index, id);
    }
}
