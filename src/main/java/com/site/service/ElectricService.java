package com.site.service;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.service.IService;
import com.site.entity.ElectricEntity;

import java.util.List;


public interface ElectricService extends IService<ElectricEntity> {
    List<ElectricEntity> math();

    Page pageQuery(Page page, ElectricEntity electricEntity);
}
