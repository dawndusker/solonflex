package com.site.service;

import com.mybatisflex.core.service.IService;
import com.site.entity.GenerationEntity;

/**
 *  服务层。
 *
 * @author meihui
 * @since 2023-06-11
 */
public interface GenerationService extends IService<GenerationEntity> {

}