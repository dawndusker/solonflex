package com.site.service.impl;

import com.mybatisflex.solon.service.impl.ServiceImpl;
import com.site.entity.GenerationEntity;
import com.site.mapper.GenerationMapper;
import com.site.service.GenerationService;
import org.noear.solon.annotation.Component;

/**
 *  服务层实现。
 *
 * @author meihui
 * @since 2023-06-11
 */
@Component
public class GenerationServiceImpl extends ServiceImpl<GenerationMapper, GenerationEntity> implements GenerationService {

}