package com.site.service.impl;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.solon.service.impl.ServiceImpl;
import com.site.entity.ElectricEntity;
import com.site.mapper.ElectricMapper;
import com.site.service.ElectricService;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;


import static com.mybatisflex.core.query.QueryMethods.*;

import java.util.List;

@Component
public class ElectricServiceImpl extends ServiceImpl<ElectricMapper,ElectricEntity> implements ElectricService {
    @Inject
    ElectricMapper electricMapper;

    @Override
    public Page<ElectricEntity> pageQuery(Page page,ElectricEntity electricEntity) {

         Page<ElectricEntity> paginate = electricMapper.paginate(page.getPageNumber(),
                 page.getPageSize(),
                 QueryWrapper.create().where(ELECTRIC_ENTITY.TYPE.eq(electricEntity.getType())));
         return paginate;
    }

    @Override
    public List<ElectricEntity> math() {
        QueryWrapper query = new QueryWrapper()
                .select(
                        ELECTRIC_ENTITY.TYPE,
                        max(ELECTRIC_ENTITY.POWER).as("powerMax"),
                        sum(ELECTRIC_ENTITY.POWER).as("powerSum"),
                        avg(ELECTRIC_ENTITY.POWER).as("powerAvg")
                ).from(ELECTRIC_ENTITY)
                .groupBy(ELECTRIC_ENTITY.TYPE);
        List<ElectricEntity> electricEntities = electricMapper.selectListByQuery(query);
        return electricEntities;
    }


}
