package com.site.utils;

import com.mybatisflex.core.row.Row;
import lombok.Data;

@Data
public class IPage {
    private Integer size=10;
    private Integer num=1;
    private Row eq;
    private Row like;
}
