package com.site.utils;

import org.noear.redisx.RedisClient;
import org.noear.snack.ONode;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;

@Component
public class RedisUtils {
    @Inject
    RedisClient redisClient;
    public <T> void put(String key, T value) {
        redisClient.getBucket().storeAndSerialize(key, value);
    }

    public <T> void put(String key, T value, int timeoutSeconds) {
        redisClient.getBucket().storeAndSerialize(key, value,timeoutSeconds);
    }

    public ONode get(String key) {
        return ONode.load(redisClient.getBucket().getAndDeserialize(key, ONode.class));
    }

}
