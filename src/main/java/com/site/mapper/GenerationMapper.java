package com.site.mapper;

import com.mybatisflex.core.BaseMapper;
import com.site.entity.GenerationEntity;

/**
 *  映射层。
 *
 * @author meihui
 * @since 2023-06-11
 */
public interface GenerationMapper extends BaseMapper<GenerationEntity> {

}
