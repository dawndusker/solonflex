package com.site.entity;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import java.io.Serializable;

import com.mybatisflex.core.keygen.KeyGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *  实体类。
 *
 * @author meihui
 * @since 2023-06-11
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(value = "t_generation")
public class GenerationEntity implements Serializable {


    @Id(keyType= KeyType.Generator, value= KeyGenerators.flexId)
    private Long id;

    
    private String generationType;

}
