package com.site.entity;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;

import lombok.*;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(value = "t_electric")
@ToString
public class ElectricEntity implements Serializable {

    @Id(keyType = KeyType.None)
    private Integer id;

    //发电
    private String type;
    
    private Integer power;

    private Long typeId;

    private LocalDate date;

    @Column(ignore = true)
    private BigDecimal powerAvg;
    @Column(ignore = true)
    private BigInteger powerSum;
    @Column(ignore = true)
    private Integer powerMax;

}
