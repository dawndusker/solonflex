package com.site.config;

import com.mybatisflex.core.mybatis.FlexConfiguration;
import com.zaxxer.hikari.HikariDataSource;
import org.noear.redisx.RedisClient;
import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;
import org.noear.solon.cache.jedis.RedisCacheService;
import org.noear.solon.data.cache.CacheService;

import javax.sql.DataSource;

@Configuration
public class DbConfig {
    @Bean
    public CacheService cache(@Inject("${url.redis}") RedisCacheService cache) {
        return cache;
    }

    @Bean
    public RedisClient redisClient(@Inject("${url.redis}") RedisClient client) {
        return client;
    }

}
